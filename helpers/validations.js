// IMPORT TYPINGS

// EXPORT VALIDATIONS
export function RequiredField() {
  return (value) => {
    let result = true
    if (!value || value === '') {
      result = 'Campo Obligatorio'
    }
    return result
  }
}

export function EmailField() {
  return (value) => {
    let result = true
    const regex =
      /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i
    if (!regex.test(value)) {
      result = 'Debe ser un correo válido'
    }
    return result
  }
}
