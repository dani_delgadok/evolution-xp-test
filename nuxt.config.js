// IMPORT LIBRARIES
import colors from 'vuetify/es5/util/colors'

// IMPORT ENV
import env from './.env.json'

// NUXT SETTINGS
export default {
  server: {
    host: env.host,
    port: env.port,
  },
  head: {
    titleTemplate: 'Evolution XP Test',
    title: 'Evolution XP Test',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content:
          'width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Evolution XP Test',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Material+Icons',
      },
    ],
  },
  css: ['~/assets/global.css'],
  plugins: [],
  components: true,
  buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/vuetify'],
  modules: ['@nuxtjs/axios', '@nuxtjs/pwa'],
  axios: {
    baseURL: env.api,
  },
  pwa: {
    manifest: {
      lang: 'en',
    },
  },
  env: {
    api: env.api,
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },
  build: {},
}
