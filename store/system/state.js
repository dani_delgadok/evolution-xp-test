// EXPORT STORE STATE
export default () => {
  return {
    loading: false,
    uploader: {
      show: false,
      file: null,
    },
    notification: {
      show: false,
      message: '',
      type: '',
    },
  }
}
