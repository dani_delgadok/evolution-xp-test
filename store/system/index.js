// IMPORT STORE MODULES
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import state from './state'

// EXPORT STORE
export default {
  actions,
  getters,
  mutations,
  state,
}
