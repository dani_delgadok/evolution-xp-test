// EXPORT GETTERS
export default {
  GetLoading: (state) => state.loading,
  GetNotification: (state) => state.notification,
  GetFileUploader: (state) => state.uploader,
}
