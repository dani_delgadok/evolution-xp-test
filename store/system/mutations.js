// EXPORT MUTATIONS
export default {
  SetLoading: (state, payload) => (state.loading = payload),
  SetNotification: (state, payload) => (state.notification = payload),
  SetFileUploader: (state, payload) => (state.uploader = payload),
}
